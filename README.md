# 120mm_Dual_8in_Adapter

Fan adapter for 120mm dual fan ASICS

120mm dual fan to 8" standard HVAC duct adapter. Should fit any any dual fan machine but was designed with Bitmain S19/S17 series in mind.

.6mm Nozzle: .7mm wide/.3mm layer height
3 perimeters/4 top and bottom layers
No supports, brim or raft
25% infill
PETG filament
Print as fast as your machine allows for good layer adhesion

.4mm Nozzle: .5mm wide/.2mm layer height
5 perimeters/6 top and bottom layers
No supports, brim or raft
25% infill
PETG
Print as fast as your machine allows for good layer adhesion
